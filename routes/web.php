<?php
Auth::routes();

// Posts
// Route::get('/projects', 'PostsController@index')->name('projects');
//
// Route::get('/posts/create', 'PostsController@create');
//
// Route::post('/posts', 'PostsController@store');
//
// Route::get('/posts/{post}', 'PostsController@show');
//
// Route::get('posts/{id}/edit', 'PostsController@edit');
//


Route::resource('posts', 'PostsController');
//Route::get('/projects', 'PostsController@index')->name('projects');
Route::view('/resume', 'static.resume');

Route::get('/',['as'=>'home', function(){
  return view('static.resume');
}]);

Route::get('contact',
  ['as' => 'contact', 'uses' => 'ContactController@create']);
Route::post('contact',
  ['as' => 'contact_store', 'uses' => 'ContactController@store']);

Route::get('/posts/tags/{tag}', 'TagsController@show');

// Route::get('/freelancer', function(){
// return 'Hello World';
// });

// Route::get('/experience',['as'=>'experience', function(){
//   return view('static.experience');
// }]);
