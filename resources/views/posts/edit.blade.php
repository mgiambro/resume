@extends('layouts.master')

@section('content')
<div class="col-sm-8 blog-main">
<h1>Edit: {!! $post->title !!}</h1>

{!! Form::model($post, ['method' => 'PATCH', 'action' => ['PostsController@update', $post->id]]) !!}
    @include('posts.partials._form', ['submitButtonText' => 'Update Post'])
{!! Form::close() !!}

@include('layouts.errors')
</div>
@stop
