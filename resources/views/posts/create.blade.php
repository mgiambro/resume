@extends('layouts.master')

@section('content')
<div class="col-sm-8 blog-main">
<h1>Write a new Post</h1>
<hr/>
@include('flash::message')
{!! Form::model($post = new \resume\Models\Post, ['url' => 'posts']) !!}
    @include('posts.partials._form', ['submitButtonText' => 'Add Post'])
{!! Form::close() !!}

@include('layouts.errors')
</div>
@endsection
