<aside class="col-sm-3 ml-sm-auto blog-sidebar">
  <!-- <div class="sidebar-module sidebar-module-inset">
      <h4>About</h4>
      <p>Some <em>stuff</em>.</p>
  </div> -->
  <div class="sidebar-module">
      <h4>Archives</h4>
      <ol class="list-unstyled">

          @foreach ($archives as $archive)
              <li>

                  <a href="{{ route('posts.index') }}/?month={{ $archive['month']}}&year={{$archive['year']}}">
                      {{ $archive['month'] . ' ' . $archive['year'] }}
                  </a>

              </li>
          @endforeach

      </ol>
  </div>
      <div class="sidebar-module">
      <h4>Tags</h4>
      <ol class="list-unstyled">

          @foreach ($tags as $tag)
              <li>

                  <a href="/posts/tags/{{ $tag }}">
                      {{ $tag }}
                  </a>

              </li>
          @endforeach

      </ol>
  </div>
  <div class="sidebar-module">
      <h4>Elsewhere</h4>
      <ol class="list-unstyled">
          <li><a href="https://bitbucket.org/account/signin/">Bitbucket</a></li>
          <li><a href="https://twitter.com/mgiambro">Twitter</a></li>
      </ol>
  </div>
</aside><!-- /.blog-sidebar -->
