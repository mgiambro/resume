@extends('layouts.master')

@section('content')
    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
        <div class="my-auto">
          <h1 class="mb-0">Maurizio
            <span class="text-primary">Giambrone</span>
          </h1>
          <div class="subheading mb-5">Bournemouth, UK ·
            <a href="{{ route('posts.index') }}">PHP Web Developer</a>
          </div>
          <p class="mb-5">I am a PHP Web Programmer with experience working on both object-oriented PHP web applications based on the Laravel framework and also applications coded in procedural PHP. On the front end I have experience using JavaScript and CSS frameworks such as Jquery, Bootstrap and ChartJS. Prior to working predominantly with PHP I worked in Java development and have a passion for writing clean, de-coupled and re-usable object oriented code.</p>
          <ul class="list-inline list-social-icons mb-0">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="experience">
        <div class="my-auto">
          <h2 class="mb-5">Experience</h2>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Freelance PHP Web Developer</h3>
              <div class="subheading mb-3">Independent</div>
              <p>Providing high quality Web Development and Support Services to a variety of clients ranging from large international companies to local startups and small businesses.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">January 2016 - Present</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">PHP Web Developer</h3>
              <div class="subheading mb-3">RLA</div>
              <p>RLA is a Marketing and Creative Advertising Agency serving mainly the Automotive Industry. My role at RLA was to develop and support PHP Laravel applications for a vartiety of clients including BMW, Volkswagen and Scania.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">September 2015 - December 2015</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">PHP Developer</h3>
              <div class="subheading mb-3">Clevertech Solutions</div>
              <p>Clevertech are a small Web Development company. My role at Clevertech was to develop and support an eBay-style bidding application which was offered as a service to customers of Barclays Bank Plc. I also developed a bulk email marketing application written in Object Oriented PHP and using the Amazon Web Services (AWS) Simple Email Service (SES) API.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">April 2014 - July 2015</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Java Engineer</h3>
              <div class="subheading mb-3">SmartFocus</div>
              <p>SmartFocus offers Cloud-based Personaalised Email Marketing. Using agile test-driven development methodologies, my role at SmartFocus was to develop and support their software as a service (SAAS) Personisation Engine. This service is a service provided to eCommerce vendors which tracks user behaviour and trends using various algorithms and offers product suggestions based on the users activity on the web.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">January 2013 - February 2014</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Ecommerce Analyst Developer</h3>
              <div class="subheading mb-3">LV=</div>
              <p>LV= are one of the United Kingdoms largest insurance companies. I worked with the business to analyse requirements, develop and support the LV= Quote and Buy Motor Insurance application.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">March 2012 - December 2012</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Lotus Notes / Java Developer</h3>
              <div class="subheading mb-3">Donovan Data Systems (DDS)</div>
              <p>Powering $140 billion in global media budgets, DDS (now MediaOcean) are a software company providing products for the Media and Advertising world. I developed a variety of web and client in-house applications using the Lotus Notes platform, PHP, JavaScript, CSS and HTML. I also contributed Java development to the range of software products the business offers.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">February 2006 - March 2012</span>
            </div>
          </div>

        </div>

      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="education">
        <div class="my-auto">
          <h2 class="mb-5">Education</h2>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Bournemouth University</h3>
              <div class="subheading mb-3">Bachelor of Science</div>
              <div>Applied Psychology and Computing</div>
              <p>Classification: 2i</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">August 1996 - May 1999</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Bournemouth and Poole College of Further Educaction</h3>
              <div class="subheading mb-3">RSA Advanced Diploma in Business Procedures</div>
              <p>Classification: Merit-Distinction</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">August 1993 - May 1994</span>
            </div>
          </div>

        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="skills">
        <div class="my-auto">
          <h2 class="mb-5">Skills</h2>

          <div class="subheading mb-3">Programming Languages &amp; Tools</div>
          <ul class="list-inline list-icons">
            <li class="list-inline-item">
              <i class="devicons devicons-php"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-laravel"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-mysql"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-amazonwebservices"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-linux"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-ssh"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-ubuntu"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-java"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-atom"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-git"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-bitbucket"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-html5"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-css3"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-javascript"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-jquery"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-sass"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-bootstrap"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-webpack"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-npm"></i>
            </li>
          </ul>

          <div class="subheading mb-3">Workflow</div>
          <ul class="fa-ul mb-0">
            <li>
              <i class="fa-li fa fa-check"></i>
              Object Oriented Design</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Test Driven Development</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Cross Functional Teams</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Agile Development &amp; Scrum</li>
          </ul>
        </div>
      </section>

    </div>

@endsection
