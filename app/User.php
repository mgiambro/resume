<?php

namespace resume;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use resume\Models\Post;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isATeamManager()
    {
        return true;
    }

    public function isMe()
    {
      $ret = false;
      if($this->name === 'Maurizio Giambrone'){
        $ret = true;
      }
      return $ret;
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }

}
