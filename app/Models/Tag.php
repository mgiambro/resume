<?php

namespace resume\Models;

class Tag extends Model
{
    protected $fillable = [
        'name'
    ];

    public function posts(){
        return $this->belongsToMany(Post::class);
    }

    public function getRouteKeyName(){
        return 'name';
    }
}
