<?php

namespace resume\Providers;

use Illuminate\Support\ServiceProvider;
use resume\Models\Post;
use resume\Models\Tag;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer('layouts.sidebar', function($view){
          $archives = Post::archives();
          $tags = Tag::has('posts')->pluck('name');

          $view->with(compact('archives','tags'));
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Bind interfaces to implementations
        $this->app->bind('resume\Repositories\Contracts\TagRepositoryInterface', 'resume\Repositories\Eloquent\TagRepository');
        $this->app->bind('resume\Repositories\Contracts\PostRepositoryInterface', 'resume\Repositories\Eloquent\PostRepository');
    }
}
