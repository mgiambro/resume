<?php

namespace resume\Repositories\Contracts;

/**
 *
 * @author maurizio
 */
interface TagRepositoryInterface {
    
     public function all();
     
}
