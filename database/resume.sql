-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19-0ubuntu0.16.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table resume.comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table resume.migrations: ~6 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2017_10_03_101930_create_posts_table', 1),
	(4, '2017_10_03_102011_create_comments_table', 1),
	(5, '2017_10_03_102030_create_tags_table', 1),
	(6, '2017_10_06_091648_add_timestamps_to_post_tag', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping data for table resume.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping data for table resume.posts: ~4 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
REPLACE INTO `posts` (`id`, `user_id`, `title`, `body`, `created_at`, `updated_at`, `published_at`) VALUES
	(1, 1, 'PGC Global', '<p>PGC Global are a US-based workforce management company offering a full range of financial, business and administrative services.</p>\r\n\r\n<p>I provided freelance web development services for their web-based buiness workflow applications which help manage the businsess processes between contarctors, recruiters and company hiring managers. The applications cover business areas including Contactor Onboarding, Payroll Tax Management and Client Billing.</p>\r\n\r\n<p>The applications are all PHP-based and hosted on Amazon EC2 servers. My contribution included many feature enhancements such as adding JavaScript web charts and reporting, various form functionality and implementing Amazon Simple Email Services (SES) classes throughout. I also provided bug fixing and support.</p>\r\n\r\n<p>Below are links to some screenshots.</p>\r\n\r\n<p><a href="http://resume.app/img/active_workforce_chart.png" onclick="window.open(this.href, \'ActiveWorkforceChart\', \'resizable=yes,status=no,location=no,toolbar=yes,menubar=no,fullscreen=no,scrollbars=no,dependent=no,width=658,height=337\'); return false;">Active Workforce Chart</a></p>\r\n\r\n<p><a href="http://resume.app/img/contract_requests_form.png" onclick="window.open(this.href, \'\', \'resizable=no,status=no,location=no,toolbar=yes,menubar=no,fullscreen=no,scrollbars=no,dependent=no,width=683,height=939\'); return false;">Contract Requests Form</a></p>\r\n\r\n<p><a href="http://resume.app/img/dashboard.png" onclick="window.open(this.href, \'\', \'resizable=no,status=no,location=no,toolbar=yes,menubar=no,fullscreen=no,scrollbars=no,dependent=no,width=960,height=580\'); return false;">Dashboard</a></p>', '2017-11-03 16:05:55', '2017-11-09 14:14:32', '2017-11-08 14:14:32'),
	(2, 1, 'Quicr', '<p>Quicr are a startup food nutrition company who provide a mobile app which scans food product barcodes and provides allergen alerts and ingredient&nbsp;informatiion in an easy to read and searchable form.</p>\r\n\r\n<p>My role on the project was to write the&nbsp;<a href="https://www.brandbank.com" target="_blank">Brandbank</a>&nbsp;API integration which populates, updates and maintains the Quicr database with product information which the mobile app references.</p>', '2017-11-03 16:06:28', '2017-11-09 14:34:16', '2017-11-07 14:34:16'),
	(4, 1, 'Olson MVC', '<p>Olson MVC is a lightweight MVC framework built by myself. It combines tried and tested third-party components with hand-coded classes to provide a light, fast and elegant MVC framework with which to base your web application. My philosohpy is to minimalise bloat. Therefore, this framework aims to comprise the main base components necessary for a web application framework. If additional functionality is required, the developer can either choose a component from&nbsp;<a href="https://packagist.org" target="_blank">packagist.org</a>&nbsp;or develop it themselves.</p>\r\n\r\n<p>The framework is available on Bitbucket where you can either clone it:</p>\r\n\r\n<p>git clone https://mgiambro@bitbucket.org/mgiambro/olsonmvc.git projectname</p>\r\n\r\n<p>or download it:</p>\r\n\r\n<p><a href="https://bitbucket.org/mgiambro/olsonmvc/downloads/" target="_blank">Olson MVC Download</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', '2017-11-10 14:47:15', '2017-11-10 15:11:23', '2017-11-06 15:11:23'),
	(5, 1, 'A Simple CMS', '<p>A very simple CMS built on Olson MVC. It provides a simple means of managing articles via an admin section and storing them in a MySQL database. It comes with a basic CSS theme, but can easily be styled to suit your requirements.</p>\r\n\r\n<p>This is essentially intended as a demo project to give interested parties an example of my coding. However, it also serves as a good starting point for developers to use when developing their own custom CMS.</p>\r\n\r\n<p>It is available on Bitbucket where you can either clone it:</p>\r\n\r\n<p>git clone https://mgiambro@bitbucket.org/mgiambro/cms.git [projectname]</p>\r\n\r\n<p>or download it:</p>\r\n\r\n<p><a href="https://bitbucket.org/mgiambro/cms/downloads/" target="_blank">A Simple CMS</a></p>', '2017-11-09 15:08:21', '2017-11-09 15:11:44', '2017-11-05 15:11:44');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping data for table resume.post_tag: ~24 rows (approximately)
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
REPLACE INTO `post_tag` (`post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2017-11-09 12:12:00', '2017-11-09 12:12:00'),
	(1, 3, '2017-11-09 13:08:02', '2017-11-09 13:08:02'),
	(1, 4, '2017-11-09 13:08:02', '2017-11-09 13:08:02'),
	(1, 5, '2017-11-09 13:08:02', '2017-11-09 13:08:02'),
	(1, 6, '2017-11-09 13:08:02', '2017-11-09 13:08:02'),
	(1, 7, '2017-11-09 13:08:02', '2017-11-09 13:08:02'),
	(1, 8, '2017-11-09 13:08:02', '2017-11-09 13:08:02'),
	(1, 9, '2017-11-09 13:55:54', '2017-11-09 13:55:54'),
	(1, 10, '2017-11-09 13:55:54', '2017-11-09 13:55:54'),
	(2, 1, '2017-11-09 14:34:16', '2017-11-09 14:34:16'),
	(2, 3, '2017-11-09 14:34:16', '2017-11-09 14:34:16'),
	(2, 10, '2017-11-09 14:34:16', '2017-11-09 14:34:16'),
	(2, 11, '2017-11-09 14:34:16', '2017-11-09 14:34:16'),
	(4, 3, '2017-11-09 14:47:15', '2017-11-09 14:47:15'),
	(4, 12, '2017-11-09 15:09:42', '2017-11-09 15:09:42'),
	(4, 13, '2017-11-09 15:09:42', '2017-11-09 15:09:42'),
	(4, 14, '2017-11-09 15:09:42', '2017-11-09 15:09:42'),
	(4, 15, '2017-11-09 15:11:23', '2017-11-09 15:11:23'),
	(5, 3, '2017-11-09 15:08:21', '2017-11-09 15:08:21'),
	(5, 7, '2017-11-09 15:08:21', '2017-11-09 15:08:21'),
	(5, 9, '2017-11-09 15:08:21', '2017-11-09 15:08:21'),
	(5, 12, '2017-11-09 15:08:21', '2017-11-09 15:08:21'),
	(5, 13, '2017-11-09 15:08:21', '2017-11-09 15:08:21'),
	(5, 15, '2017-11-09 15:11:44', '2017-11-09 15:11:44');
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;

-- Dumping data for table resume.tags: ~15 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
REPLACE INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Freelance', '2017-11-09 11:28:37', '2017-11-09 11:28:39'),
	(2, 'Laravel', '2017-11-09 11:28:56', '2017-11-09 11:28:57'),
	(3, 'PHP', '2017-11-09 13:04:41', '2017-11-09 13:04:43'),
	(4, 'Jquery', '2017-11-09 13:05:04', '2017-11-09 13:05:05'),
	(5, 'ChartJS', '2017-11-09 13:05:22', '2017-11-09 13:05:23'),
	(6, 'JavaScript', '2017-11-09 13:05:42', '2017-11-09 13:05:43'),
	(7, 'HTML', '2017-11-09 13:05:54', '2017-11-09 13:05:55'),
	(8, 'Amazon Web Services (AWS)', '2017-11-09 13:06:23', '2017-11-09 13:06:24'),
	(9, 'CSS', '2017-11-09 13:38:08', '2017-11-09 13:38:09'),
	(10, 'API Integration', '2017-11-09 13:38:11', '2017-11-09 13:38:12'),
	(11, 'SOAP', '2017-11-09 13:38:23', '2017-11-09 13:38:24'),
	(12, 'Symfony', '2017-11-09 14:45:08', '2017-11-09 14:45:09'),
	(13, 'Component-based', '2017-11-09 14:45:27', '2017-11-09 14:45:28'),
	(14, 'Composer', '2017-11-09 14:45:39', '2017-11-09 14:45:40'),
	(15, 'Home Projects', '2017-11-09 15:09:29', '2017-11-09 15:09:31');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping data for table resume.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Maurizio Giambrone', 'm_giambrone@hotmail.co.uk', '$2y$10$zgcmkrJmRnURmOviZ3Ha4eZ3wYDD.KS29fSeNB.2jgzgXEnBSphVK', NULL, '2017-11-03 16:01:50', '2017-11-03 16:01:50');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
